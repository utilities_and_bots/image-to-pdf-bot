# image-to-pdf-bot

telegram bot that converts jpeg and png images to pdf file

## installation
1. install [rust](https://www.rust-lang.org/learn/get-started) and python
2. make new directory (`mkdir image-to-pdf`)
3. clone this repository there
4. clone repository [image-to-pdf-converter](https://codeberg.org/jogang/image-to-pdf-converter) to the same folder
5. make venv in converter folder
6. install converter requiremens
7. cd to bot repo directory
8. create **.env** file with lines  `TELOXIDE_TOKEN=<your telegram bot token>` and `CONVERTER_FOLDER=<path to the folder of image-to-pdf-converter (should end with /)>`
9. execute `cargo run`