use teloxide::{
    dispatching::dialogue::InMemStorage,
    prelude::Dialogue,
    requests::Requester,
    types::{InputFile, Message},
    Bot,
};

use crate::types_and_consts::*;

type MyDialogue = Dialogue<State, InMemStorage<State>>;
type HandlerResult = Result<(), Box<dyn std::error::Error + Send + Sync>>;

pub async fn confirm_queue(
    bot: Bot,
    dialogue: MyDialogue,
    files: Vec<String>,
    msg: Message,
) -> HandlerResult {
    dialogue
        .update(State::Sending {
            files: files.clone(),
        })
        .await?;
    let output_path = get_pdf_path(&files, msg.chat.username().unwrap_or("0"));
    let output_path_buf = PathBuf::from(&output_path);

    let mut doc: InputFile = InputFile::file(output_path_buf);
    let title = "Converted PDF ".to_string() + msg.chat.username().unwrap_or("") + ".pdf";
    doc = doc.file_name(title);
    bot.send_document(msg.chat.id, doc).await?;

    delete_files(files, output_path).await;

    dialogue.update(State::Start).await?;
    Ok(())
}

use std::{path::PathBuf, process::Command, env};
fn get_pdf_path(files: &Vec<String>, username: &str) -> String {
    let converter_folder = env::var("CONVERTER_FOLDER").unwrap();
    let python_path = converter_folder.to_string() + "venv/bin/python3";
    let script_path = converter_folder.to_string() + "src/main.py";

    let mut cmd = Command::new(python_path);
    cmd.arg(script_path);
    for x in files {
        cmd.arg(x);
    }
    let required_output_path = "/tmp/convertedPdf".to_string() + username + ".pdf";
    cmd.arg(&required_output_path);

    let output_path = String::from_utf8(cmd.output().unwrap().stdout).unwrap();

    println!("processed {:?}", output_path);
    return output_path;
}

async fn delete_files(files: Vec<String>, file: String) {
    for x in files {
        tokio::spawn(async move {
            let _ = tokio::fs::remove_file(&x).await;
        });
    }
    let _ = tokio::fs::remove_file(file).await;
}
