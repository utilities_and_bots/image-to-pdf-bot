use crate::{shema::schema, types_and_consts::*};
use dotenv::dotenv;
use teloxide::{
    dispatching::{dialogue::*, *},
    prelude::*,
};

mod downloader;
mod image_processor;
mod pdf_constructor;
mod receiver;
mod shema;
mod simple_bot_functions;
mod types_and_consts;
mod buttons;

#[tokio::main]
async fn main() {
    dotenv().ok();
    let bot = Bot::from_env();

    check_dependencies();

    println!("started bot");

    Dispatcher::builder(bot, schema())
        .dependencies(dptree::deps![InMemStorage::<State>::new()])
        .enable_ctrlc_handler()
        .build()
        .dispatch()
        .await;
}

fn check_dependencies() {
    use std::{env, path::Path};
    let mut all_fine = true;

    let converter_folder = env::var("CONVERTER_FOLDER").unwrap_or("-1".to_string());

    if converter_folder == "-1" {
        println!("Укажите директорию image-to-pdf-converter");
    } else {
        let python_path = converter_folder.to_string() + "venv/bin/python3";
        let script_path = converter_folder.to_string() + "src/main.py";
        let folder_exists = Path::new(&converter_folder).is_dir();
        let python_exists = Path::new(&python_path).exists();
        let script_exists = Path::new(&script_path).exists();

        if !folder_exists || !python_exists || !script_exists {
            all_fine = false;
        }

        if !folder_exists {
            println!("Укажите верную директорию image-to-pdf-converter");
        }
        if !python_exists {
            println!("Создайте venv и установите зависимости");
        }
        if !script_exists {
            println!("Проверьте правильность пути: скрипт не найден");
        }

        let bot_token = env::var("TELOXIDE_TOKEN").unwrap_or("-1".to_string());
        if bot_token == "-1" {
            println!("Укажите токен бота");
            all_fine = false;
        }
    }
    if !all_fine {
        panic!("Ошибка перед запуском бота");
    }
}
