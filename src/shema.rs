use teloxide::{
    dispatching::{
        dialogue::{self, InMemStorage},
        UpdateFilterExt, UpdateHandler,
    },
    dptree,
    types::Update,
};

use crate::{pdf_constructor, receiver, simple_bot_functions::*, types_and_consts::*};

pub fn schema() -> UpdateHandler<Box<dyn std::error::Error + Send + Sync + 'static>> {
    use dptree::case;

    let command_state_handler = Update::filter_message()
        .branch(case![State::Adding { files }].endpoint(pdf_constructor::confirm_queue));

    let command_handler = teloxide::filter_command::<Command, _>()
        .branch(case![Command::Start].endpoint(receiver::start_adding))
        .branch(case![Command::Cancel].endpoint(cancel))
        .branch(command_state_handler);

    let message_handler = Update::filter_message()
        .branch(command_handler)
        .branch(case![State::Start].endpoint(helper))
        .branch(case![State::Adding { files }].endpoint(receiver::receive_files))
        .branch(case![State::Sending { files }])
        .endpoint(wait);

    dialogue::enter::<Update, InMemStorage<State>, State, _>().branch(message_handler)
}
