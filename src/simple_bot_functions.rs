use teloxide::{
    dispatching::dialogue::InMemStorage,
    prelude::Dialogue,
    requests::{Requester},
    types::{Message},
    Bot,
};

use crate::types_and_consts::*;

type MyDialogue = Dialogue<State, InMemStorage<State>>;
type HandlerResult = Result<(), Box<dyn std::error::Error + Send + Sync>>;

pub async fn helper(bot: Bot, msg: Message) -> HandlerResult {
    let help_text = format!("Чтобы начать добавление команд в очередь введите команду /start",);

    bot.set_chat_menu_button().await?;
    bot.send_message(msg.chat.id, help_text).await?;
    Ok(())
}
pub async fn cancel(bot: Bot, dialogue: MyDialogue, msg: Message) -> HandlerResult {
    dialogue.update(State::Start).await?;
    bot.send_message(msg.chat.id, "Обработка прервана").await?;
    Ok(())
}
pub async fn wait(bot: Bot, msg: Message) -> HandlerResult {
    bot.send_message(msg.chat.id, "Подождите").await?;
    Ok(())
}
