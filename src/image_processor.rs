use std::collections::HashMap;

use teloxide::{prelude::*, types::PhotoSize};

use crate::types_and_consts::*;

pub async fn get_photos_metadata(bot: &Bot, raw: &Vec<&PhotoSize>) -> Vec<Image> {
    let mut result = Vec::new();
    let mut mid_results = HashMap::new();

    for x in raw {
        let id = (x.file.id).to_string();
        let size = (x.file.size).to_owned();

        if mid_results.get(&id).unwrap_or(&0) < &size {
            mid_results.insert(id, size);
        }
    }
    for x in mid_results.keys() {
        let cur_image = Image {
            id: x.to_string(),
            path: bot.get_file(x).await.unwrap().path,
        };
        result.push(cur_image);
    }

    return result;
}
