use std::{ffi::OsStr, path::Path};

use teloxide::{net::Download, Bot};

use crate::types_and_consts::*;

fn get_extension_from_filename(filename: &str) -> Option<&str> {
    Path::new(filename).extension().and_then(OsStr::to_str)
}

pub async fn get_downloaded_paths(bot: &Bot, photos_meta: &Vec<Image>) -> Vec<String> {
    let mut result = Vec::new();
    for x in photos_meta {
        let extension = get_extension_from_filename(&x.path).unwrap();

        let path = "/tmp/".to_string() + &x.id + "." + extension;
        let mut destination = tokio::fs::File::create(&path).await.unwrap();
        let _ = bot.download_file(&x.path, &mut destination).await;
        result.push(path);
    }
    return result;
}
