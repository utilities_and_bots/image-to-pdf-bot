use teloxide::macros::BotCommands;

#[derive(Debug)]
pub struct Image {
    pub(crate) id: String,
    pub(crate) path: String,
}

pub const SUPPORTED_IMAGES: [&str; 4] = ["jpeg", "png", "bmp", "pdf"];

#[derive(Clone, Default, Debug)]
pub enum State {
    #[default]
    Start,
    Adding {
        files: Vec<String>,
    },
    Sending {
        files: Vec<String>,
    },
}

#[derive(BotCommands, Clone)]
#[command(rename_rule = "lowercase", description = "Поддерживаемые команды:")]
pub enum Command {
    #[command(description = "Отобразить помощь")]
    Help,
    #[command(description = "Начать добавление фалов в очередь на обработку")]
    Start,
    #[command(description = "Очистить очередь")]
    Cancel,
    #[command(description = "Завершить добавление файлов и начать обработку")]
    Confirm,
}
