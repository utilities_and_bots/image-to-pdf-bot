use teloxide::{dispatching::dialogue::InMemStorage, prelude::*};

use crate::{downloader::get_downloaded_paths, image_processor::*, types_and_consts::*};

type MyDialogue = Dialogue<State, InMemStorage<State>>;
type HandlerResult = Result<(), Box<dyn std::error::Error + Send + Sync>>;

pub async fn start_adding(bot: Bot, dialogue: MyDialogue, msg: Message) -> HandlerResult {
    dialogue.update(State::Adding { files: vec![] }).await?;
    send_files_count_info(&bot, &Vec::new(), &msg).await;
    Ok(())
}

pub async fn receive_files(
    bot: Bot,
    dialogue: MyDialogue,
    files: Vec<String>,
    msg: Message,
) -> HandlerResult {
    let mut photos_meta = Vec::new();
    let mut request_error = String::new();
    if msg.photo().is_some() {
        photos_meta = get_photos(&bot, &msg).await;
    } else if let Some(_) = document_photo_extension(&msg) {
        let doc_request = get_document_as_photo(&bot, &msg).await;
        if doc_request.is_ok() {
            photos_meta = doc_request.unwrap();
        } else {
            request_error = doc_request.unwrap_err();
        }
    } else {
        bot.send_message(msg.chat.id, "Отправьте фотографии")
            .await?;
    }

    if photos_meta.len() > 0 {
        let downloaded_photos = get_downloaded_paths(&bot, &photos_meta).await;
        let mut new_files = files.clone();
        for x in downloaded_photos {
            new_files.push(x.clone());
        }
        send_files_count_info(&bot, &new_files, &msg).await;
        dialogue.update(State::Adding { files: new_files }).await?;
    } else if !request_error.is_empty() {
        send_error_message(&bot, &msg, &request_error).await;
    }

    Ok(())
}

fn document_photo_extension(msg: &Message) -> Option<String> {
    let result;
    if msg.document().is_some() {
        let subtype = msg
            .document()
            .unwrap()
            .mime_type
            .as_ref()
            .unwrap()
            .subtype()
            .to_string();
        if SUPPORTED_IMAGES.contains(&&subtype[..]) {
            result = Some(subtype);
        } else {
            result = None;
        }
    } else {
        result = None;
    }
    return result;
}
async fn get_photos(bot: &Bot, msg: &Message) -> Vec<Image> {
    let mut raw = msg.photo().unwrap().to_vec();
    raw.sort_by(|a, b| (&a.file.size.cmp(&b.file.size)).to_owned());
    let last = vec![raw.last().unwrap()];
    let photos_meta = get_photos_metadata(&bot, &last).await;
    return photos_meta;
}
async fn get_document_as_photo(bot: &Bot, msg: &Message) -> Result<Vec<Image>, String> {
    let id = &msg.document().unwrap().file.id;
    let file_request = &bot.get_file(&msg.document().unwrap().file.id).await;
    if file_request.is_ok() {
        let path = &file_request.as_ref().unwrap().path;
        let photos_meta = vec![Image {
            id: id.to_string(),
            path: path.to_string(),
        }];
        return Ok(photos_meta);
    } else {
        let a = file_request.as_ref().unwrap_err().to_string();
        return Err(a);
    }
}
async fn send_files_count_info(bot: &Bot, files: &Vec<String>, msg: &Message) {
    let text = format!("Файлов в очереди: {}", files.len());
    let _ = bot.send_message(msg.chat.id, text).await;
}
async fn send_error_message(bot: &Bot, msg: &Message, err: &String) {
    let text = format!("{}", err);
    let _ = bot.send_message(msg.chat.id, text).await;
}
